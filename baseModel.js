'use strict'

class baseModel {
	constructor (){
	}
	static findAll(){
		return baseModel.logger.log(this.predicate);
	}
}



export default (logger) => {
	if(baseModel.logger) return baseModel
	baseModel.logger = logger
	return baseModel
}
