'use strict'

import baseModel from './baseModel'

class User extends baseModel() {
  constructor(){
    super()
  }

  static findAll(){
    return super.findAll()
  }
}

User.predicate = 'user'


export default User
