'use strict'

var Logger = {
	log: (name) => {
		return `Yes I\'m the logger for ${name}s`
	}
}


import base from './baseModel'
base(Logger)

import user from './user'
console.log(user.findAll())

import language from './language'
console.log(language.findAll())
