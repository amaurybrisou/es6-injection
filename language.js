'use strict'

import baseModel from './baseModel'

class User extends baseModel() {
  constructor(){
    super()
  }

  static findAll(){
    return super.findAll()
  }
}

User.predicate = 'language'


export default User
